# Firmware AP-B Versión:01.00

Firmware para Semáforos del Sistema de Activación del Personal de Bomberos

**Características Generales**: Streaming de Audio por UDP, Tramas por TCPIP (Stack v5.36). Manejo de dos Potencias de Audio y Salidas Auxiliares

**Hardware**: PIC18F97J60 - Placa Madre Versión 3.04

**IDE**: MPLABX 5.05 - C18 V3.47 Lite

**Aplicación WEB**: https://github.com/Shelvak/firehouse

**Licencia**: The MIT License

	Copyright © 2013-2018 FRM-UTN Fernando Marotta

	Permission is hereby granted, free of charge, to any person obtaining a copy of this firmware and associated documentation files (the 'Firmware'), to deal in the Firmware without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Firmware, and to permit people to whom the Firmware is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Firmware.

	THE FIRMWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE FIRMWARE OR THE USE OR OTHER DEALINGS IN THE FIRMWARE.