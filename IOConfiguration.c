/* 
 * File:   IOConfiguration.c
 * Author: Agustin Gonzalez / Fernando Marotta
 *
 * Created on 1 de Noviembre de 2018, 11:35
 */


#include "Config_Version_and_ID.h"
#include "GenericTypeDefs.h"
#include "IOConfiguration.h"
#include "HardwareProfile.h"


/******************** Definitions *********************************************/
enum{
    OUTPUT = 0,
    INPUT
};

enum{
    LOW = 0,
    HI
};
/******************** Public Prototype Functions ******************************/
/*
 void PreinitializeIO(void);
*/

/******************** Private Prototype Functions *****************************/
void Initialize_PORTA();
void Initialize_PORTB();
void Initialize_PORTC();
void Initialize_PORTD();
void Initialize_PORTE();
void Initialize_PORTF();
void Initialize_PORTG();
void Initialize_PORTH();
void Initialize_PORTJ();
/******************** Extern Variables ****************************************/

/******************** Globals Variables ***************************************/ 

/******************** Private Funcionts ***************************************/

void Initialize_PORTA(){
    //RA0 LED_A
    //RA1 LED_B
TRISAbits.TRISA2 = OUTPUT;
LATAbits.LATA2 = LOW;
TRISAbits.TRISA3 = OUTPUT;
LATAbits.LATA3 = LOW;
TRISAbits.TRISA4 = OUTPUT;
LATAbits.LATA4 = LOW;
    //RA5 WARNING POT 1
}
void Initialize_PORTB(){
    //RB0 DREG
    //RB1 XDCS
    //RB2 XCS
TRISBbits.TRISB3 = OUTPUT;
LATBbits.LATB3 = LOW;  
#ifndef MOVILYDOTACION
TRISBbits.TRISB4 = OUTPUT;
LATBbits.LATB4 = LOW;    
TRISBbits.TRISB5 = OUTPUT;
LATBbits.LATB5 = LOW;    
#endif
//RB6 PGC  
    //RB7 PGD
}
void Initialize_PORTC(){
TRISCbits.TRISC0 = OUTPUT;
LATCbits.LATC0 = LOW;  
    //RC1 PWM1
    //RC2 PWM3
    //RC3 SCK
    //RC4 MISO
    //RC5 MOSI
    //RC6 TX
    //RC7 RX
}
void Initialize_PORTD(){
    //RD0 XRST
TRISDbits.TRISD1 = OUTPUT;
LATDbits.LATD1 = LOW;  
TRISDbits.TRISD2 = OUTPUT;
LATDbits.LATD2 = LOW;  
TRISDbits.TRISD3 = OUTPUT;
LATDbits.LATD3 = LOW;  
TRISDbits.TRISD4 = OUTPUT;
LATDbits.LATD4 = LOW;  
TRISDbits.TRISD5 = OUTPUT;
LATDbits.LATD5 = LOW;  
TRISDbits.TRISD6 = OUTPUT;
LATDbits.LATD6 = LOW;  
TRISDbits.TRISD7 = OUTPUT;
LATDbits.LATD7 = LOW;  
}

void Initialize_PORTE(){
#ifndef CHICHARRA
TRISEbits.TRISE0 = OUTPUT;
LATEbits.LATE0 = LOW;  
#endif
#ifndef SEMAFORO_BOX1
TRISEbits.TRISE1 = OUTPUT;
LATEbits.LATE1 = LOW;  
#endif
TRISEbits.TRISE2 = OUTPUT;
LATEbits.LATE2 = LOW;  
TRISEbits.TRISE3 = OUTPUT;
LATEbits.LATE3 = LOW;  
    //RE4 CS_M1
    //RE5 CS_M2
#ifndef LUZPASILLO
TRISEbits.TRISE6 = OUTPUT;
LATEbits.LATE6 = LOW;
#endif
#ifndef HABILITAR_ENTRADAS
#ifndef LUZPASILLO
TRISEbits.TRISE7 = OUTPUT;
LATEbits.LATE7 = LOW;  
#endif
#endif
}
void Initialize_PORTF(){
#ifndef CHICHARRA
TRISFbits.TRISF0 = OUTPUT;
LATFbits.LATF0 = LOW;  
#endif
TRISFbits.TRISF1 = OUTPUT;
LATFbits.LATF1 = LOW;  
TRISFbits.TRISF2 = OUTPUT;
LATFbits.LATF2 = LOW;  
TRISFbits.TRISF3 = OUTPUT;
LATFbits.LATF3 = LOW;  
TRISFbits.TRISF4 = OUTPUT;
LATFbits.LATF4 = LOW;  
TRISFbits.TRISF5 = OUTPUT;
LATFbits.LATF5 = LOW;  
TRISFbits.TRISF6 = OUTPUT;
LATFbits.LATF6 = LOW;  
TRISFbits.TRISF7 = OUTPUT;
LATFbits.LATF7 = LOW;  
}
void Initialize_PORTG(){
    //RG0 PWM4
#ifndef MOVILYDOTACION
TRISGbits.TRISG1 = OUTPUT;
LATGbits.LATG1 = LOW; 
#endif
TRISGbits.TRISG2 = OUTPUT;
LATGbits.LATG2 = LOW; 
    //RG3 PWM2
    //RG4 PWM0
#ifndef TRAMA_API
TRISGbits.TRISG5 = OUTPUT;
LATGbits.LATG5 = LOW;
TRISGbits.TRISG6 = OUTPUT;
LATGbits.LATG6 = LOW;
#endif
TRISGbits.TRISG7 = OUTPUT;
LATGbits.LATG7 = LOW; 
}
void Initialize_PORTH(){

#ifndef SEMAFORO_BOX1
TRISHbits.TRISH0 = OUTPUT;
LATHbits.LATH0 = LOW;
TRISHbits.TRISH1 = OUTPUT;
LATHbits.LATH1 = LOW;  
TRISHbits.TRISH2 = OUTPUT;
LATHbits.LATH2 = LOW;  
TRISHbits.TRISH3 = OUTPUT;
LATHbits.LATH3 = LOW;  
#endif
TRISHbits.TRISH4 = OUTPUT;
LATHbits.LATH4 = LOW;  
#ifndef DOBLE_AUDIO
    //RH5 WARNING POT 2
TRISHbits.TRISH5 = OUTPUT;
LATHbits.LATH5 = LOW;  
    //RH6 STANDBY POT 2
TRISHbits.TRISH6 = OUTPUT;
LATHbits.LATH6= LOW;  
#endif
TRISHbits.TRISH7 = OUTPUT;
LATHbits.LATH7 = LOW;  
}
void Initialize_PORTJ(){
TRISJbits.TRISJ0 = OUTPUT;
LATJbits.LATJ0 = LOW;   
TRISJbits.TRISJ1 = OUTPUT;
LATJbits.LATJ1 = LOW; 
#ifndef MOVILYDOTACION
TRISJbits.TRISJ2 = OUTPUT;
LATJbits.LATJ2 = LOW;   
TRISJbits.TRISJ3 = OUTPUT;
LATJbits.LATJ3 = LOW;
#endif
TRISJbits.TRISJ4 = OUTPUT;
LATJbits.LATJ4 = LOW; 
TRISJbits.TRISJ5 = OUTPUT;
LATJbits.LATJ5 = LOW; 
TRISJbits.TRISJ6 = OUTPUT;
LATJbits.LATJ6 = LOW; 
    //RJ7 STANDBY POT 1
}


/******************** Public Functions ****************************************/


/*********************************************************************
 * Function:        void PreinitializeIO(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        Initialize Ports void IO .
 *
 * Note:            None
 ********************************************************************/
void PreinitializeIO(void){

Initialize_PORTA();
Initialize_PORTB();
Initialize_PORTC();
Initialize_PORTD();
Initialize_PORTE();
Initialize_PORTF();
Initialize_PORTG();
Initialize_PORTH();
Initialize_PORTJ();

}
