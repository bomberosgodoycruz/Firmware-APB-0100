
/*********************************************************************
 *
 *  Generic TCP Client Example Application
 *  Module for Microchip TCP/IP Stack
 *   -Implements an example HTTP client and should be used as a basis
 *     for creating new TCP client applications
 *    -Reference: None.  Hopefully AN833 in the future.
 *
 *********************************************************************
 * FileName:        GenericTCPClient.c
 * Dependencies:    TCP, DNS, ARP, Tick
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F, PIC32
 * Compiler:        Microchip C32 v1.05 or higher
 *               Microchip C30 v3.12 or higher
 *               Microchip C18 v3.30 or higher
 *               HI-TECH PICC-18 PRO 9.63PL2 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * Copyright (C) 2002-2009 Microchip Technology Inc.  All rights
 * reserved.
 *
 * Microchip licenses to you the right to use, modify, copy, and
 * distribute:
 * (i)  the Software when embedded on a Microchip microcontroller or
 *      digital signal controller product ("Device") which is
 *      integrated into Licensee's product; or
 * (ii) ONLY the Software driver source files ENC28J60.c, ENC28J60.h,
 *      ENCX24J600.c and ENCX24J600.h ported to a non-Microchip device
 *      used in conjunction with a Microchip ethernet controller for
 *      the sole purpose of interfacing with the ethernet controller.
 *
 * You should refer to the license agreement accompanying this
 * Software for additional information regarding your rights and
 * obligations.
 *
 * THE SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT
 * WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT
 * LIMITATION, ANY WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL
 * MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF
 * PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
 * BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE
 * THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER
 * SIMILAR COSTS, WHETHER ASSERTED ON THE BASIS OF CONTRACT, TORT
 * (INCLUDING NEGLIGENCE), BREACH OF WARRANTY, OR OTHERWISE.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     8/01/06   Original
 ********************************************************************/
#define __GENERICTCPCLIENT_C

#include "TCPIPConfig.h"

#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)


#include <TCPIP.h>
#include <string.h>
#include "flash.h"
#include "MP3Client.h"
#include "math.h"
#include "Dotacion_Movil.h"
#include "Command_Parser.h"
#include "Dotacion_Movil.h"
#include "Command_Execution.h"
#include "Config_Version_and_ID.h"
#include "InputHandler.h"
 
/********************* Definitions ********************************************/


/******************** Public Prototype Functions ******************************/
void GenericTCPClient(void);

/******************** Private Prototype Functions *****************************/

/******************** Extern Variables ****************************************/
extern BOOL play_streaming;
extern BOOL Play_IR;
extern unsigned int PWM_ON_OFF_P_DN_R[11];


/******************** Globals Variables ***************************************/ 
static BYTE ServerName[] = "server.bvgc.org";
static WORD ServerPort = 9800;
WORD conectado=0;
char estado_previo_entradas;
#ifdef CHICHARRA
DWORD timer_chicharra;
BOOL chicharra_flag=0;
#endif
DWORD PERSONA_ATRAPADA=0,T=0;
unsigned int MUX=0;
BOOL playf=0;
int inicio=1;
int NO_GRABAR=1;
// Defines the port to be accessed for this application
char RxBuffer[MAX_COMMANDS_RX][MAX_LENGTH_COMMAND]={0};
char KEEP_ALIVE[9]={0};//>S(000)<

/*****************************************************************************
  Function:
   void GenericTCPClient(void)

  Summary:
   Implements a simple socket TCP

  Description:
   This function implements a simple socket TCP client, which operates over TCP.

  Precondition:
   TCP is initialized.

  Parameters:
   None

  Returns:
     None
  ***************************************************************************/
void GenericTCPClient(void)
{
   BYTE i,index=0,cont_commands=0;
   static BIT  flagstart=0,flag_trama_nueva=0;
   
   WORD w, w2;
   BYTE AppBuffer[34];
   WORD wMaxGet, wMaxPut, wCurrentChunk;
             
   static DWORD Timer;
   
   static TCP_SOCKET   My_Socket = INVALID_SOCKET;
   static enum _GenericTCPExampleState
   {
      SM_HOME = 0,
      SM_SOCKET_OBTAINED,
      SM_LISTENING,
      SM_DISCONNECT
   } GenericTCPExampleState = SM_HOME;


    /* Restore saved last ALS before Power Off*/
    if(inicio==1){   
        inicio=0;
        Command_Parser_Restore_Saved_State();
    }


   /* multiplexado DISPLAY 7SEG */
    #ifdef MOVILYDOTACION
    if(MUX==0){
        MUX=1;
        Display_Dotacion(PWM_ON_OFF_P_DN_R[1]);
    }else if(MUX==1){
        MUX=0;
        Display_Movil(PWM_ON_OFF_P_DN_R[2]); 
        }
    #endif
   /* Toggle LEDS*/
   Command_Execution_ALS_PERSONA_ATRAPADA(PWM_ON_OFF_P_DN_R[8]);
   

   switch(GenericTCPExampleState)
   {
      case SM_HOME:
         // Connect a socket to the remote TCP server
         My_Socket = TCPOpen((DWORD)&ServerName[0], TCP_OPEN_RAM_HOST, ServerPort, TCP_PURPOSE_GENERIC_TCP_CLIENT);
         // Abort operation if no TCP socket of type TCP_PURPOSE_GENERIC_TCP_CLIENT is available
         // If this ever happens, you need to go add one to TCPIPConfig.h
         if(My_Socket == INVALID_SOCKET)
            break;
         GenericTCPExampleState++;
         Timer = TickGet();
         break;


      case SM_SOCKET_OBTAINED:
         // Wait for the remote server to accept our connection request
         if(!TCPIsConnected(My_Socket))
         {
            // Time out if too much time is spent in this state
            if(TickGet()-Timer > 5*TICK_SECOND)
            {
               // Close the socket so it can be used by other modules
               TCPDisconnect(My_Socket);
               My_Socket = INVALID_SOCKET;
               GenericTCPExampleState--;
            }
            break;
         }

         Timer = TickGet();
         // Make certain the socket can be written to
         if(TCPIsPutReady(My_Socket) < 125u)
            break;
         GenericTCPExampleState++;
         break;


       case SM_LISTENING:
         if(!TCPIsConnected(My_Socket))
         {
            GenericTCPExampleState = SM_DISCONNECT;
         }
         //TCPPutROMString(MySocket, (ROM BYTE*)">Dentro del listening<");
         //TCPFlush(MySocket);

         // Figure out how many bytes have been received and how many we can transmit.
         wMaxGet = TCPIsGetReady(My_Socket);   // Get TCP RX FIFO byte count

         // Process all bytes that we can
         // This is implemented as a loop, processing up to sizeof(AppBuffer) bytes at a time.
         // This limits memory usage while maximizing performance.  Single byte Gets and Puts are a lot slower than multibyte GetArrays and PutArrays.
         wCurrentChunk = sizeof(AppBuffer);
         cont_commands=0;
         index=0;
         for(w = 0; w < wMaxGet; w += sizeof(AppBuffer)){
            // Make sure the last chunk, which will likely be smaller than sizeof(AppBuffer), is treated correctly.
            if(w + sizeof(AppBuffer) > wMaxGet)
               wCurrentChunk = wMaxGet - w;

            // Transfer the data out of the TCP RX FIFO and into our local processing buffer.
            TCPGetArray(My_Socket, AppBuffer, wCurrentChunk);
  
            for(w2 = 0; w2 < wCurrentChunk; w2++){
                if (AppBuffer[w2] == '>')
                  {
                  flagstart=1;                  
                  }

               if(flagstart)
                  {
                  RxBuffer[cont_commands][index]=AppBuffer[w2];
                  if (index < MAX_LENGTH_COMMAND)
                    index++;
                  }
                if (AppBuffer[w2] == '<')
                   {
                    RxBuffer[cont_commands][index]='\0';
                    flag_trama_nueva=1;                    
                   }
                if ( flag_trama_nueva ){
                    flag_trama_nueva=0;
                    flagstart=0;
                    Command_Parser_Parser_Command(My_Socket,RxBuffer[cont_commands],index);
                    cont_commands++;
                    if(cont_commands > MAX_COMMANDS_RX) cont_commands = 0;
                    index=0;
                }    
            }   
         }//sale del for

        
            if(TickGet() - Timer >= 5*TICK_SECOND)///2ul)
            {
                Timer=TickGet();
                sprintf(KEEP_ALIVE,">S(%03u)<",ID);
                TCPPutArray(My_Socket,KEEP_ALIVE, sizeof KEEP_ALIVE - 1);
                TCPFlush(My_Socket);
            }
            #ifdef HABILITAR_ENTRADAS
            if(estado_previo_entradas != LeerEntradas()){
                EnviarInputs(My_Socket, estado_previo_entradas );
                estado_previo_entradas = LeerEntradas();
            }
            #endif

            #ifdef CHICHARRA
            if(chicharra_flag==1 && (TickGet() - timer_chicharra >= TICK_SECOND/2))///2ul)
            {
              chicharra_flag=0;
              CHICHARRA_O = 0;
              CHICHARRA_MIRROR_O = 0;
            }
            #endif
         

      break;

      case SM_DISCONNECT:
         TCPDisconnect(My_Socket);
         My_Socket = INVALID_SOCKET;
         GenericTCPExampleState = SM_HOME;
        break;
   }//Sale del switch
 
}



#endif   //#if defined(STACK_USE_GENERIC_TCP_CLIENT_EXAMPLE)

