Microchip Technology Inc., June 2011

The Internet Radio demo project which used to work with the SHOUTcast service is no longer supported.

Toward the end of 2010, the SHOUTcast directory service changed the web API interface to version 2.0 which requires the use of a Developer-ID (Dev-ID). However at the time of this software release, SHOUTcast does not currently issue any new Dev-ID, making it impossible to update the firmware to work with the new interface.

Therefore, Microchip can no longer fully support this demo or the following related collaterals:

- Application Note AN1128 (DS01128)
- Internet Radio Demonstration Board (DM183033)

It may still be possible to use the Internet Radio Demonstration Board with a different firmware to work with other services. However, Microchip does not currently provide an alternative firmware for other services.

As the situation changes in the future, it maybe possible to re-introduce the SHOUTcast support.

