/* 
 * File:   Command_Execution.c
 * Author: Fernando Marotta
 *
 * Created on 7 de octubre de 2018, 01:15
 */
#include "Config_Version_and_ID.h"
#include "GenericTypeDefs.h"
#include "Tick.h"
#include "Leds_Pwm.h"
#include "Command_Parser.h"
#include "Command_Execution.h"
#include "HardwareProfile.h"
#include "flash.h"
#include "math.h"
#include "VLSICodec.h"
#include <string.h>


/******************** Definitions *********************************************/
enum {
ROJO = 1,
VERDE,
AMARILLO,
AZUL,
BLANCO
};


/******************** Public Prototype Functions ******************************/
/*
void Command_Execution_ALS (TCP_SOCKET MySocket, char * CDU , int indice, int ACK_Y_GRABAR );
void Command_Execution_PWM (TCP_SOCKET MySocket, char * CDU , int indice );
 */
/******************** Private Prototype Functions *****************************/
void Command_Execution_ALS_REPOSO( void );
void Command_Execution_ALS_DIA( void  );
void Command_Execution_ALS_NOCHE( void );

/******************** Extern Variables ****************************************/
extern unsigned int PWM_ON_OFF_P_DN_R[11];
extern unsigned int PWM_ON_OFF_P_DN_R_TEMP[11];
extern unsigned int PWM_DUTY_R[5];//0 a 100
extern unsigned int PWM_DUTY_D_MIN[5];//0 a 100
extern unsigned int PWM_DUTY_N_MAX[5];// 0 a 100
extern DWORD PERSONA_ATRAPADA;
extern BOOL play_streaming;

#ifdef CHICHARRA
extern DWORD timer_chicharra;
extern BOOL chicharra_flag;
#endif

/******************** Globals Variables ***************************************/ 
int Toggle=0;
/******************** Private Funcionts ***************************************/
void Command_Execution_ALS_REPOSO ( ){
    if(PWM_ON_OFF_P_DN_R[3] == 1){//ROJO
        Leds_Pwm_Set_Duty_PWM( ROJO ,PWM_DUTY_R[0]);
        }
    if(PWM_ON_OFF_P_DN_R[4] == 1){//VERDE
        Leds_Pwm_Set_Duty_PWM( VERDE ,PWM_DUTY_R[1]);                
        }
    if(PWM_ON_OFF_P_DN_R[5] == 1){//AMARILLO
        Leds_Pwm_Set_Duty_PWM( AMARILLO,PWM_DUTY_R[2]);
        }
    if(PWM_ON_OFF_P_DN_R[6] == 1){//AZUL
        Leds_Pwm_Set_Duty_PWM(AZUL,PWM_DUTY_R[3]);
        }
    if(PWM_ON_OFF_P_DN_R[7] == 1){//BLANCO
        Leds_Pwm_Set_Duty_PWM(BLANCO,PWM_DUTY_R[4]);
        }
}
void Command_Execution_ALS_DIA ( ){
    if(PWM_ON_OFF_P_DN_R[3] == 1){//ROJO
        Leds_Pwm_Set_Duty_PWM( ROJO , PWM_DUTY_D_MIN[0] );
        }
    if(PWM_ON_OFF_P_DN_R[4] == 1){//VERDE
        Leds_Pwm_Set_Duty_PWM( VERDE , PWM_DUTY_D_MIN[1] );
        }
    if(PWM_ON_OFF_P_DN_R[5] == 1){//AMARILLO
        Leds_Pwm_Set_Duty_PWM( AMARILLO , PWM_DUTY_D_MIN[2] );                
        }
    if(PWM_ON_OFF_P_DN_R[6] == 1){//AZUL
        Leds_Pwm_Set_Duty_PWM( AZUL , PWM_DUTY_D_MIN[3] );
        }
    if(PWM_ON_OFF_P_DN_R[7] == 1){//BLANCO
        Leds_Pwm_Set_Duty_PWM( BLANCO , PWM_DUTY_D_MIN[4] );
        }
}

void Command_Execution_ALS_NOCHE ( ){
    if(PWM_ON_OFF_P_DN_R[3] == 1){//ROJO
        Leds_Pwm_Set_Duty_PWM( ROJO , PWM_DUTY_N_MAX[0] );
        }
    if(PWM_ON_OFF_P_DN_R[4] == 1){//VERDE
        Leds_Pwm_Set_Duty_PWM( VERDE , PWM_DUTY_N_MAX[1] );
        }
    if(PWM_ON_OFF_P_DN_R[5] == 1){//AMARILLO
        Leds_Pwm_Set_Duty_PWM( AMARILLO , PWM_DUTY_N_MAX[2] );
        }
    if(PWM_ON_OFF_P_DN_R[6] == 1){//AZUL
        Leds_Pwm_Set_Duty_PWM( AZUL , PWM_DUTY_N_MAX[3] );
        }
    if(PWM_ON_OFF_P_DN_R[7] == 1){//BLANCO
        Leds_Pwm_Set_Duty_PWM( BLANCO , PWM_DUTY_N_MAX[4] );
        }
}



/******************** Public Functions ****************************************/

void Command_Execution_ALS (TCP_SOCKET MySocket, char * CDU , int indice, int ACK_Y_GRABAR ){
    char MEM_TEMP[64]={0};
    char alsok[9]={">ALSOK<\0"};
    int i;
    
    
        if(CDU[indice-12] > 0 && CDU[indice-12] <100){//Prioridad
            PWM_ON_OFF_P_DN_R[0] = CDU[indice-12];
                if(ACK_Y_GRABAR==1){  
                    #ifdef CHICHARRA
                    if(PWM_ON_OFF_P_DN_R[0] == 1){
                        timer_chicharra = TickGet();
                        chicharra_flag=1;
                        CHICHARRA_O = 1;
                        CHICHARRA_MIRROR_O = 1;
                        }
                    #endif
                    }
                }
        if(CDU[indice-11] > 0 && CDU[indice-11] < 10 ){//DOTACION
            PWM_ON_OFF_P_DN_R[1] = CDU[indice-11];
            }
        if(CDU[indice-10] > 0 && CDU[indice-10] < 21 ){//Movil
            PWM_ON_OFF_P_DN_R[2] = CDU[indice-10];
            }

        if(CDU[indice-9] == 1){
            PWM_ON_OFF_P_DN_R[3] =1;
            #ifdef SEMAFORO_BOX1
            BOX_ROJO_O = 1; 
            #endif
            }
        if(CDU[indice-9] == 0){//B1: ROJO
            PWM_ON_OFF_P_DN_R[3] =0;
            Leds_Pwm_Off( ROJO );
            #ifdef SEMAFORO_BOX1
            BOX_ROJO_O = 0; 
            #endif
            }
        if(CDU[indice-8] == 1){
            PWM_ON_OFF_P_DN_R[4] =1;
            #ifdef SEMAFORO_BOX1
            BOX_VERDE_O = 1; 
            #endif
            }
        if(CDU[indice-8] == 0){//B2: VERDE
            PWM_ON_OFF_P_DN_R[4] =0;
            Leds_Pwm_Off( VERDE );            
            #ifdef SEMAFORO_BOX1
            BOX_VERDE_O = 0; 
            #endif
            }
        if(CDU[indice-7] == 1){
            PWM_ON_OFF_P_DN_R[5] =1;
            #ifdef SEMAFORO_BOX1
            BOX_AMARILLO_O = 1; 
            #endif
            }
        if(CDU[indice-7] == 0){//B4: AMARILLO
            PWM_ON_OFF_P_DN_R[5] =0;
            Leds_Pwm_Off( AMARILLO );
            #ifdef SEMAFORO_BOX1
            BOX_AMARILLO_O = 0; 
            #endif
            }
        if(CDU[indice-6] == 1){
            PWM_ON_OFF_P_DN_R[6] =1;
            #ifdef SEMAFORO_BOX1
            BOX_AZUL_O = 1; 
            #endif
            }
        if(CDU[indice-6] == 0){//B5: AZUL
            PWM_ON_OFF_P_DN_R[6] =0;
            Leds_Pwm_Off( AZUL );
            #ifdef SEMAFORO_BOX1
            BOX_AZUL_O = 0; 
            #endif
            }
        if(CDU[indice-5] == 1){
            PWM_ON_OFF_P_DN_R[7] =1;
            #ifdef SEMAFORO_BOX1
            BOX_BLANCO_O = 1; 
            #endif
            }
        if(CDU[indice-5] == 0){//B3: BLANCO
            PWM_ON_OFF_P_DN_R[7] =0;
            Leds_Pwm_Off( BLANCO );            
            #ifdef SEMAFORO_BOX1
            BOX_BLANCO_O = 0; 
            #endif
            }
    

        if(CDU[indice-4] == 1){PWM_ON_OFF_P_DN_R[8] =1;}
        if(CDU[indice-4] == 0){PWM_ON_OFF_P_DN_R[8] =0;}//B6: PERSONA ATRAPADA
        if(CDU[indice-3] == 1){PWM_ON_OFF_P_DN_R[9] =1;}
        if(CDU[indice-3] == 0){PWM_ON_OFF_P_DN_R[9] =0;}//B7: DIA=1 NOCHE=0
    
        if(CDU[indice-2] == 1){
            PWM_ON_OFF_P_DN_R[10]=1;//B8: REPOSO
            #ifdef LUZPASILLO
                LUZPASILLO1_O = 0;
                LUZPASILLO2_O = 0;           
            #endif            
        }
    
        if(CDU[indice-2] == 0){
            PWM_ON_OFF_P_DN_R[10]=0;//B8: REPOSO
            #ifdef LUZPASILLO
            if(PWM_ON_OFF_P_DN_R[9]== 0 && (PWM_ON_OFF_P_DN_R[3]==1 || PWM_ON_OFF_P_DN_R[4]==1 || PWM_ON_OFF_P_DN_R[5]==1 || PWM_ON_OFF_P_DN_R[6] ==1 || PWM_ON_OFF_P_DN_R[7]==1)){//NOCHE
                LUZPASILLO1_O = 1;
                LUZPASILLO2_O = 1;           
                }
            #endif
        }

        if(PWM_ON_OFF_P_DN_R[9]== 1 && PWM_ON_OFF_P_DN_R[10]==0){//DIA
            Command_Execution_ALS_DIA();            
            }
    
        if(PWM_ON_OFF_P_DN_R[9]== 0 && PWM_ON_OFF_P_DN_R[10]==0){//NOCHE
            Command_Execution_ALS_NOCHE();             
            }
        if(PWM_ON_OFF_P_DN_R[10]==1){//REPOSO
            Command_Execution_ALS_REPOSO();
            }
    if(ACK_Y_GRABAR==1){  
        TCPPutArray(MySocket,alsok,strlen(alsok) );
        TCPFlush(MySocket);
              
        ReadFlash(65408,64,MEM_TEMP);
        EraseFlash(65408,65439);//borrar 32 posicion (64bytes)
        for(i=0;i<=10;i++){MEM_TEMP[i]=PWM_ON_OFF_P_DN_R[i];}//escribir los primeros 11 bytes de
        WriteBytesFlash(65408,64,MEM_TEMP);
        }

}


void Command_Execution_PWM(TCP_SOCKET MySocket,char* CDU, int indice){
 char MEM_TEMP[64]={0};
 char pwmok[9]={">PWMOK<\0"};
 unsigned long duty_temp=0;
 
 if(CDU[indice-6]< 6 && CDU[indice-6] > 0)//COLOR
        {
            //sprintf(LCDText,">PWMCRLH<       %s",CDU);LCDUpdate();
            if(CDU[indice-5] >= 0 && CDU[indice-5]<=101 )
                {
                /*LEER 64 bytes (32 words) DE MEMORIA Y GUARDAR EN TEMPORAL*/
                ReadFlash(65408,64,MEM_TEMP);
         
                /*FIN LEER 64 BYTES DE MEMORIA*/
                EraseFlash(65408,65439);//borrar 32 posicion (64bytes)

                duty_temp = CDU[indice-5]  ;

                switch (CDU[indice-6])//color
                    {
                    case 1://ROJO
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 1 && CDU[indice-2] == 0)//(R1 = 0) AND (L1 = 1) AND (H1 = 0) configura nivel minimo para dia
                    {
                    PWM_DUTY_D_MIN[0]=duty_temp;
                    MEM_TEMP[11]=PWM_DUTY_D_MIN[0];
                    }
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 0 && CDU[indice-2] == 1)//(R1 = 0) AND (L1 = 0) AND (H1 = 1) configura nivel maximo para noche
                    {
                    PWM_DUTY_N_MAX[0]=duty_temp;                    
                    MEM_TEMP[16]=PWM_DUTY_N_MAX[0];
                    }
                    if(CDU[indice-4] == 1)//(R1 = 1) AND (L1 = x) AND (H1 = x) configura nivel de reposo
                    {
                    PWM_DUTY_R[0]=duty_temp;
                    MEM_TEMP[21]=PWM_DUTY_R[0];
                    }
                    
                    break;    
                    case 2:
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 1 && CDU[indice-2] == 0)//(R1 = 0) AND (L1 = 1) AND (H1 = 0) configura nivel minimo para dia
                    {
                    PWM_DUTY_D_MIN[1]=duty_temp;
                    MEM_TEMP[12]=PWM_DUTY_D_MIN[1];
                    }
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 0 && CDU[indice-2] == 1)//(R1 = 0) AND (L1 = 0) AND (H1 = 1) configura nivel maximo para noche
                    {
                    PWM_DUTY_N_MAX[1]=duty_temp;
                    MEM_TEMP[17]=PWM_DUTY_N_MAX[1];
                    }
                    if(CDU[indice-4] == 1)//(R1 = 1) AND (L1 = x) AND (H1 = x) configura nivel de reposo
                    {
                    PWM_DUTY_R[1]=duty_temp;
                    MEM_TEMP[22]=PWM_DUTY_R[1];
                    }
                    break;
                    case 3:
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 1 && CDU[indice-2] == 0)//(R1 = 0) AND (L1 = 1) AND (H1 = 0) configura nivel minimo para dia
                    {
                    PWM_DUTY_D_MIN[2]=duty_temp;
                    MEM_TEMP[13]=PWM_DUTY_D_MIN[2];
                    }
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 0 && CDU[indice-2] == 1)//(R1 = 0) AND (L1 = 0) AND (H1 = 1) configura nivel maximo para noche
                    {
                    PWM_DUTY_N_MAX[2]=duty_temp;
                    MEM_TEMP[18]=PWM_DUTY_N_MAX[2];
                    }
                    if(CDU[indice-4] == 1)//(R1 = 1) AND (L1 = x) AND (H1 = x) configura nivel de reposo
                    {
                    PWM_DUTY_R[2]=duty_temp;
                    MEM_TEMP[23]=PWM_DUTY_R[2];
                    }
                    break;
                    case 4:
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 1 && CDU[indice-2] == 0)//(R1 = 0) AND (L1 = 1) AND (H1 = 0) configura nivel minimo para dia
                    {
                    PWM_DUTY_D_MIN[3]=duty_temp;
                    MEM_TEMP[14]=PWM_DUTY_D_MIN[3];
                    }
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 0 && CDU[indice-2] == 1)//(R1 = 0) AND (L1 = 0) AND (H1 = 1) configura nivel maximo para noche
                    {
                    PWM_DUTY_N_MAX[3]=duty_temp;                    
                    MEM_TEMP[19]=PWM_DUTY_N_MAX[3];
                    }
                    if(CDU[indice-4] == 1)//(R1 = 1) AND (L1 = x) AND (H1 = x) configura nivel de reposo
                    {
                    PWM_DUTY_R[3]=duty_temp;
                    MEM_TEMP[24]=PWM_DUTY_R[3];
                    }
                    break;
                    case 5:
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 1 && CDU[indice-2] == 0)//(R1 = 0) AND (L1 = 1) AND (H1 = 0) configura nivel minimo para dia
                    {
                    PWM_DUTY_D_MIN[4]=duty_temp;
                    MEM_TEMP[15]=PWM_DUTY_D_MIN[4];
                    }
                    if(CDU[indice-4] == 0 &&  CDU[indice-3] == 0 && CDU[indice-2] == 1)//(R1 = 0) AND (L1 = 0) AND (H1 = 1) configura nivel maximo para noche
                    {
                    PWM_DUTY_N_MAX[4]=duty_temp;
                    MEM_TEMP[20]=PWM_DUTY_N_MAX[4];
                    }
                    if(CDU[indice-4] == 1)//(R1 = 1) AND (L1 = x) AND (H1 = x) configura nivel de reposo
                    {
                    PWM_DUTY_R[4]=duty_temp;
                    MEM_TEMP[25]=PWM_DUTY_R[4];
                    }
                    break;
                    default:
                    break;
                }

                WriteBytesFlash(65408,64,MEM_TEMP);
                }
        }
        
        TCPPutArray(MySocket,pwmok, strlen(pwmok));
        TCPFlush(MySocket);

}

void Command_Execution_PRESENTACION (TCP_SOCKET MySocket){
    char presentacion[26];
    sprintf(presentacion,">#SEMAFORO[V%1u.%1u.%1u]-(%03u)<",VER1,VER2,VER3,ID);
    TCPPutArray(MySocket,presentacion, strlen(presentacion));
    TCPFlush(MySocket);
}

void Command_Execution_ALS_PERSONA_ATRAPADA(int P_Atrapada){
int i;
char TramaValida[30];
TCP_SOCKET MySocket = INVALID_SOCKET;

if (P_Atrapada){
    for(i=0;i<11;i++){PWM_ON_OFF_P_DN_R_TEMP[i]=PWM_ON_OFF_P_DN_R[i];}
    if(TickGet()-PERSONA_ATRAPADA >= TICK_SECOND/4)
        {
        PERSONA_ATRAPADA=TickGet();
        for(i=0;i<11;i++){TramaValida[i+4]=PWM_ON_OFF_P_DN_R[i];}
        if(Toggle==0)
            {
            Toggle=1;
            TramaValida[0]='>';TramaValida[1]='A';TramaValida[2]='L';TramaValida[3]='S';
            TramaValida[4]=PWM_ON_OFF_P_DN_R[0];
            TramaValida[5]=PWM_ON_OFF_P_DN_R[1];
            TramaValida[6]=PWM_ON_OFF_P_DN_R[2];
            TramaValida[16-9]=0;
            TramaValida[16-8]=0;
            TramaValida[16-7]=0;
            TramaValida[16-6]=0;
            TramaValida[16-5]=0;
            TramaValida[12]=PWM_ON_OFF_P_DN_R[8];
            TramaValida[13]=PWM_ON_OFF_P_DN_R[9];
            TramaValida[14]=PWM_ON_OFF_P_DN_R[10];
             TramaValida[15]='<';TramaValida[16]=0;
            }
        else
            {
            Toggle=0;
            TramaValida[0]='>';TramaValida[1]='A';TramaValida[2]='L';TramaValida[3]='S';
            for(i=0;i<11;i++){TramaValida[i+4]=PWM_ON_OFF_P_DN_R[i];}
            TramaValida[15]='<';TramaValida[16]=0;
            }
        
        Command_Execution_ALS(MySocket,TramaValida,16, FALSE );
        
        for(i=0;i<11;i++){PWM_ON_OFF_P_DN_R[i]=PWM_ON_OFF_P_DN_R_TEMP[i];}
        }    
    }
}

       

void Command_Execution_VOL ( TCP_SOCKET MySocket,char* CDU, int indice ){
    float volumen=0;
    char volok[9]={">VOLOK<\0"};
    if(CDU[indice-2] >= 0 && CDU[indice-2] <= 100){
        if(CDU[indice-2]>0){
            volumen = log10((float)CDU[indice-2])*100 /2; //log x * 100 2
            volumen = 255 - (volumen*255/100);
        }
        else{
            volumen=255;
        }
        VLSI_SetVolume(volumen,volumen);
        TCPPutArray( MySocket, volok, strlen(volok));
        TCPFlush( MySocket );           
        }
}

void Command_Execution_API(TCP_SOCKET MySocket,char * CDU ,int indice){

    char apiok[9]={">APIOK<\0"};
    #ifdef TRAMA_API
    if(CDU[indice-2] == 1 ){
        SIRENA_MAYOR_O = 1;
        }
    else{
        SIRENA_MAYOR_O = 0;
        }

    if(CDU[indice-3] == 1 ){
        SEMAF_COLON_O = 1;
        }
    else{
        SEMAF_COLON_O = 0;
        }
    #endif
    TCPPutArray( MySocket, apiok, strlen(apiok));
    TCPFlush( MySocket );           
    }
    
   

void Command_Execution_NOT_FOUND(TCP_SOCKET MySocket){
    char invalid_command[19] = {">INVALID_COMMAND<\0"};
    TCPPutArray( MySocket, invalid_command, strlen(invalid_command) );
    TCPFlush( MySocket );  
}
/******************** Api Initialization **************************************/
/*
API_COMMAND_EXECUTION Command_Execution = {
ALS = Command_Execution_ALS,
PWM = Command_Execution_PWM
};
*/

