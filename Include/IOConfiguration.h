/* 
 * File:   IOConfiguration.c
 * Author: Agustin Gonzalez / Fernando Marotta
 *
 * Created on 1 de Noviembre de 2018, 12:08
 */

#ifndef IOCONFIGURATION_H
#define	IOCONFIGURATION_H


/******************** Public Prototype Functions ******************************/
void PreinitializeIO(void);
/******************************************************************************/

#endif	/* IOCONFIGURATION_H */

