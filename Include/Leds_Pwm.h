/* 
 * File:   Leds_Pwm.h
 * Author: Fernando Marotta
 *
 * Created on 6 de octubre de 2018, 21:43
 */

#ifndef LEDS_PWM_H
#define	LEDS_PWM_H


/******************** Public Prototype Functions ******************************/
void Leds_Pwm_Init_PWM( void );
void Leds_Pwm_Set_Duty_PWM(unsigned int CUAL, unsigned long DUTY);
void Leds_Pwm_Off(unsigned int CUAL);
void Leds_Pwm_On(unsigned int CUAL);
/********************* Api Structure With Fucntions Pointers ******************/
/*
typedef struct _API_LEDS_PWM {
void ( *Init_PWM ) (void) ;
void ( *Set_Duty_PWM )  (unsigned int CUAL, unsigned long DUTY);
}const API_LEDS_PWM;

extern API_LEDS_PWM Leds_Pwm;
*/
#endif	/* LEDS_PWM_H */

