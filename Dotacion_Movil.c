/* 
 * File:   Dotacion_Movil.c
 * Author: Fernando Marotta
 *
 * Created on 7 de octubre de 2018, 00:48
 */
#include "Config_Version_and_ID.h"
#include "Dotacion_Movil.h"
#include "HardwareProfile.h"

/******************** Definitions *********************************************/

/******************** Public Prototype Functions ******************************/
/*
void Display_Dotacion(unsigned int num);
void Display_Movil(unsigned int num);
*/
/******************** Private Prototype Functions *****************************/

/******************** Globals Variables ***************************************/ 
 
/******************** Private Funcionts ***************************************/

/******************** Public Functions ****************************************/
void Display_Dotacion(unsigned int num)
    {
    #ifdef MOVILYDOTACION
    MUXIN_TRIS = 0; MUXIN_O =1;
    BCD3_TRIS =0;
    BCD2_TRIS =0;
    BCD1_TRIS =0;
    BCD0_TRIS =0;
    switch (num)
        {
        case 0:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=0;//0
        break;
        case 1:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=1;//1
        break;
        case 2:
        BCD3_O =0; BCD2_O =0; BCD1_O=1; BCD0_O=0;//2
        break;
        case 3:
        BCD3_O =0; BCD2_O =0; BCD1_O=1; BCD0_O=1;//3            
        break;
        case 4:
        BCD3_O =0; BCD2_O =1; BCD1_O=0; BCD0_O=0;//4            
        break;
        case 5:
        BCD3_O =0; BCD2_O =1; BCD1_O=0; BCD0_O=1;//5            
        break;
        case 6:
        BCD3_O =0; BCD2_O =1; BCD1_O=1; BCD0_O=0;//6            
        break;
        case 7:
        BCD3_O =0; BCD2_O =1; BCD1_O=1; BCD0_O=1;//7            
        break;
        case 8:
        BCD3_O =1; BCD2_O =0; BCD1_O=0; BCD0_O=0;//8
        break;
        case 9:
        BCD3_O =1; BCD2_O =0; BCD1_O=0; BCD0_O=1;//9            
        break;
        default:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=0;//0
        break;
        }
    #endif
    }


void Display_Movil(unsigned int num)
    {
    #ifdef MOVILYDOTACION
    MUXIN_TRIS =0; MUXIN_O =0; 
    /*
    BCD0: RJ3
    BCD1: RJ2
    BCD2: RB5
    BCD3: RB4
    MUX: RG1
     */
    switch (num)
        {
        case 0:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=0;//0
        break;
        case 1:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=1;//1
        break;
        case 2:
        BCD3_O =0; BCD2_O =0; BCD1_O=1; BCD0_O=0;//2
        break;
        case 3:
        BCD3_O =0; BCD2_O =0; BCD1_O=1; BCD0_O=1;//3            
        break;
        case 4:
        BCD3_O =0; BCD2_O =1; BCD1_O=0; BCD0_O=0;//4            
        break;
        case 5:
        BCD3_O =0; BCD2_O =1; BCD1_O=0; BCD0_O=1;//5            
        break;
        case 6:
        BCD3_O =0; BCD2_O =1; BCD1_O=1; BCD0_O=0;//6            
        break;
        case 7:
        BCD3_O =0; BCD2_O =1; BCD1_O=1; BCD0_O=1;//7            
        break;
        case 8:
        BCD3_O =1; BCD2_O =0; BCD1_O=0; BCD0_O=0;//8
        break;
        case 9:
        BCD3_O =1; BCD2_O =0; BCD1_O=0; BCD0_O=1;//9            
        break;
        default:
        BCD3_O =0; BCD2_O =0; BCD1_O=0; BCD0_O=0;//0
        break;
        }
    #endif
    }



/******************** Api Initialization **************************************/
/*
API_DISPLAY_7SEG Display = {
Dotacion = Display_Dotacion,
Movil    = Display_Movil,
};
*/
