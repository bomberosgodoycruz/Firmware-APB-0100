/* 
 * File:   Command_Execution.h
 * Author: Fernando Marotta
 *
 * Created on 7 de octubre de 2018, 01:15
 */

#ifndef COMMAND_EXECUTION_H
#define	COMMAND_EXECUTION_H
#include "TCPIP.h"
#include "TCP.h"

/******************** Public Prototype Functions ******************************/
void Command_Execution_ALS (TCP_SOCKET MySocket, char * CDU , int indice, int ACK_Y_GRABAR );
void Command_Execution_PWM (TCP_SOCKET MySocket, char * CDU , int indice );
void Command_Execution_PRESENTACION(TCP_SOCKET MySocket);
void Command_Execution_VOL ( TCP_SOCKET MySocket,char* CDU, int indice );
void Command_Execution_ALS_PERSONA_ATRAPADA(int Persona_Atrapada);
void Command_Execution_API(TCP_SOCKET MySocket,char * CDU ,int indice);
void Command_Execution_NOT_FOUND(TCP_SOCKET MySocket);
//CDU: Command Data Unit. Carga �til
//indice: cantidad de bytes recibidos
//MySocket: identificador del socket
/********************* Api Structure With Fucntions Pointers ******************/
/*
typedef struct _API_COMMAND_EXECUTION {
void  (* ALS) (TCP_SOCKET MySocket, char * CDU, int index, int ACK_Y_GRABAR);
void  (* PWM) (TCP_SOCKET MySocket, char * CDU, int index);
}const API_COMMAND_EXECUTION;

extern API_COMMAND_EXECUTION Command_Execution;
*/
#endif	/* COMMAND_EXECUTION_H */

